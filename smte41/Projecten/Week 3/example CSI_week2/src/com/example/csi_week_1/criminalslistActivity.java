package com.example.csi_week_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class criminalslistActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.criminalslist);

		// Get a reference to the listview
		ListView listview = (ListView) findViewById(R.id.listView1);
		// Get a reference to the list with names
		final String[] criminals = getResources().getStringArray(R.array.names);
		// Create an adapter that feeds the data to the listview
		listview.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, criminals));

		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String name = criminals[position];
				Intent intent = new Intent(criminalslistActivity.this, MainActivity.class);
				intent.putExtra("name",name);
				startActivity(intent);
				finish();

			}
		});
	}
}