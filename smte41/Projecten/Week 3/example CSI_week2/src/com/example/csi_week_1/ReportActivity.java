package com.example.csi_week_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ReportActivity extends Activity {
	@Override 
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		//Set the corresponding layout.
		setContentView(R.layout.report_activity);
		Button report = (Button) findViewById(R.id.button5);
		report.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ReportActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
			}
		});
	}

}
