package com.example.scubaassist;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

public class MainActivity extends Activity {

	// Declare Tab Variable
	ActionBar.Tab TabMap, TabDuikstekken, TabRecensie;
	Fragment fragmentTabDuikstekken = new FragmentTabDuikstekken();
	Fragment fragmentTabMap = new FragmentTabMap();
	Fragment fragmentTabRecensie = new FragmentTabRecensie();
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ActionBar actionBar = getActionBar();
		
		// Hide Actionbar Icon
		actionBar.setDisplayShowHomeEnabled(false);

		// Hide Actionbar Title
		actionBar.setDisplayShowTitleEnabled(false);

		// Create Actionbar Tabs
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		// Set tab icon and titles
		TabMap = actionBar.newTab().setText("Map");
		TabDuikstekken = actionBar.newTab().setText("Duikstek");
		TabRecensie = actionBar.newTab().setText("Recensie");
		
		// Set tab listeners
		TabMap.setTabListener(new TabListener(fragmentTabMap));
		TabDuikstekken.setTabListener(new TabListener(fragmentTabDuikstekken));
		TabRecensie.setTabListener(new TabListener(fragmentTabRecensie));
		
		// Add tabs to actionbar
		actionBar.addTab(TabMap);
		actionBar.addTab(TabDuikstekken);
		actionBar.addTab(TabRecensie);
	}
}
