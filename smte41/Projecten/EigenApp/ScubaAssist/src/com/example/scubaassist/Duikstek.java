/**
 * 
 */
package com.example.scubaassist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * @author Mike
 *
 */
public class Duikstek extends Activity {
	
	// JSON node keys
		private static final String TAG_PLAATS = "plaatsnaam";
		private static final String TAG_RATING = "rating";
		private static final String TAG_WEER = "weer";
		private static final String TAG_TEMP = "temperatuur";
	
		@Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.fragmentduikstek);
	        
	        // getting intent data
	        Intent in = getIntent();
	        
	        // Get JSON values from previous intent
	        String plaatsnaam = in.getStringExtra(TAG_PLAATS);
	        String rating = in.getStringExtra(TAG_RATING);
	        String weer = in.getStringExtra(TAG_WEER);
	        String temperatuur = in.getStringExtra(TAG_TEMP);

	        
	        // Displaying all values on the screen
	        TextView lblPlaats = (TextView) findViewById(R.id.plaats_label);
	        TextView lblRating = (TextView) findViewById(R.id.rating_label);
	        TextView lblWeer = (TextView) findViewById(R.id.weer_label);
	        TextView lblTemp = (TextView) findViewById(R.id.temp_label);
	        
	        lblPlaats.setText(plaatsnaam);
	        lblRating.setText(rating);
	        lblWeer.setText(weer);
	        lblTemp.setText(temperatuur);

	    }
}
